package com.epam.internal.course.ws.soap.entity;

public enum BookResponseParams {

    BOOK_ID("bookId"), 
    BOOK_NAME("bookName"), 
    BOOK_DESCRIPTION("bookDescription"), 
    BOOK_LANGUAGE("bookLanguage"), 
    BOOK("book");

    private String param;

    private BookResponseParams(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return param;
    }
}
