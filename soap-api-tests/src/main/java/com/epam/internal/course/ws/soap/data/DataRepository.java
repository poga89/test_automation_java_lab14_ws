package com.epam.internal.course.ws.soap.data;

import com.epam.internal.course.ws.soap.entity.GenreEntity;

/**
 * DataRepository class which holds different sets of data.
 */
public final class DataRepository {
    private static volatile DataRepository instance = null;

    private DataRepository() {
    }

    public static DataRepository get() {
        if (instance == null) {
            synchronized (DataRepository.class) {
                if (instance == null) {
                    instance = new DataRepository();
                }
            }
        }
        return instance;
    }

    public GenreEntity getNewGenre(){
        return new GenreEntity(5, "Test genre name", "Test genre description");
    }
    
    public GenreEntity updateNewGenre(){
        return new GenreEntity(5, "Test genre name - updated", "Test genre description - updated");
    }

}