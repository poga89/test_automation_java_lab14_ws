package com.epam.internal.course.ws.soap.entity;

public enum GenreRequestBodyParams {

    ORDER_TYPE("orderType"), 
    PAGE("page"), 
    PAGINATION("pagination"), 
    SIZE("size"), 
    SEARCH("search"), 
    ASC("asc"), 
    DESC("desc"),
    TRUE("true"), 
    FALSE("false"),
    GENRE("genre"),
    GENRE_ID("genreId"), 
    GENRE_NAME("genreName"), 
    GENRE_DESCRIPTION("genreDescription"),
    GET_GENRE_REQUEST("getGenreRequest"),
    GET_GENRES_REQUEST("getGenresRequest"),
    CREATE_GENRE_REQUEST("createGenreRequest"),
    UPDATE_GENRE_REQUEST("updateGenreRequest"),
    DELETE_GENRE_REQUEST("deleteGenreRequest");

    private String param;

    private GenreRequestBodyParams(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return param;
    }
}
