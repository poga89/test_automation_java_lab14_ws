package com.epam.internal.course.ws.soap.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.internal.course.ws.soap.entity.BookEntity;
import com.epam.internal.course.ws.soap.entity.ResponseErrorMessage;
import com.epam.internal.course.ws.soap.utils.ReadProjectProperties;
import com.epam.internal.course.ws.soap.utils.RegexUtils;
import com.epam.internal.course.ws.soap.utils.RsponseParser;

import io.qameta.allure.Step;

public class BookService {
    //
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final ReadProjectProperties READ_PROJECT_PROPERTIES = new ReadProjectProperties();
    //
    private static String soapEndpointUrl = READ_PROJECT_PROPERTIES.getSoapEndpointUrl();
    //
    private SOAPConnectionFactory soapConnectionFactory;
    private SOAPConnection soapConnection;
    private SOAPMessage soapResponse;
    private ResponseErrorMessage responseErrorMessage;

    public BookService() {
    }

    // getBook
    @Step(value = "get a book by id {0}")
    public BookEntity getBookById(int id) {
        logger.info(this.getClass().getSimpleName() + " start getBookById()");
        BookEntity bookEntity = new BookEntity();
        try {
            // Create SOAP Connection
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
            logger.info(this.getClass().getSimpleName() + " Create SOAP Connection for getting a book by id");

            // Send SOAP Message to SOAP Server
            soapResponse = soapConnection.call(SOAPRequest.createSOAPRequestSearchBookByNumber(soapEndpointUrl + BookEndpoints.GET_BOOK.toString(), String.valueOf(id)),
                    soapEndpointUrl);
            logger.info(this.getClass().getSimpleName() + " Send SOAP Message to SOAP Server for getting a book by id");

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            String strMsg = new String(out.toByteArray());
            logger.info(this.getClass().getSimpleName() + " response: " + strMsg);
            //
            bookEntity = RsponseParser.getBook(strMsg);
            if(bookEntity.getBookId()<0) {
                responseErrorMessage = new ResponseErrorMessage(RegexUtils.getErrorText(strMsg));
                logger.error(this.getClass().getSimpleName() + " response status: " + RegexUtils.getErrorText(strMsg));
            }
            //

            soapConnection.close();
            logger.info(this.getClass().getSimpleName() + " soapConnection.close()");
        } catch (Exception e) {
            logger.error(
                    "\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return bookEntity;
    }

    // getBooks
    @Step(value = "get a list of books")
    public List<BookEntity> getBooks() {
        logger.info(this.getClass().getSimpleName() + " start getBooks()");
        List<BookEntity> bookEntities = new ArrayList<BookEntity>();
        try {
            // Create SOAP Connection
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
            logger.info(this.getClass().getSimpleName() + " Create SOAP Connection for getting a list of books");

            // Send SOAP Message to SOAP Server
            soapResponse = soapConnection.call(SOAPRequest.createSOAPRequestGetListOfBooks(soapEndpointUrl + BookEndpoints.GET_BOOKS.toString()),
                    soapEndpointUrl);
            logger.info(this.getClass().getSimpleName() + " Send SOAP Message to SOAP Server for getting a list of books");

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            FileOutputStream fos = new FileOutputStream(new File(READ_PROJECT_PROPERTIES.getMessageDataFilePath()));
            out.writeTo(fos);
            String strMsg = new String(out.toByteArray());
            //
            logger.info(this.getClass().getSimpleName() + " Response: " + strMsg);
//            bookEntities = new ReadXmlDomParser().getListOfBooks();
            bookEntities = RsponseParser.getListOfBooks(strMsg);
            //

            soapConnection.close();
            logger.info(this.getClass().getSimpleName() + " soapConnection.close()");
        } catch (Exception e) {
            logger.error(
                    "\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return bookEntities;
    }
    
    public String getResponseErrorMessage() {
        return responseErrorMessage.getErrorMessage();
    }
}