package com.epam.internal.course.ws.soap.services;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.internal.course.ws.soap.entity.BookEntity;
import com.epam.internal.course.ws.soap.entity.BookRequestBodyParams;
import com.epam.internal.course.ws.soap.entity.GenreEntity;
import com.epam.internal.course.ws.soap.entity.GenreRequestBodyParams;
import com.epam.internal.course.ws.soap.utils.ReadProjectProperties;

import io.qameta.allure.Step;

public class SoapEnvelope {
    //
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final ReadProjectProperties READ_PROJECT_PROPERTIES = new ReadProjectProperties();
    //
    private static String myNamespace = READ_PROJECT_PROPERTIES.getMyNamespace();
    private static String myNamespaceURI = READ_PROJECT_PROPERTIES.getMyNamespaceUri();

    private SOAPBody soapBody;
    private SOAPMessage soapMessage;

    public SoapEnvelope(SOAPMessage soapMessage) {
        this.soapMessage = soapMessage;
    }

    private void initSoapEnvelope() throws SOAPException {
        logger.info(this.getClass().getSimpleName() + " start initSoapEnvelope()");
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();
        envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
        soapBody = envelope.getBody();
    }

    @Step(value = "create SoapEnvelope to Get a list of Books")
    public void createSoapEnvelopeGetBooks() throws SOAPException {
        logger.info(this.getClass().getSimpleName() + " start createSoapEnvelopeGetBooks()");
        initSoapEnvelope();
        //
//        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lib="libraryService">
//        <soapenv:Header/>
//        <soapenv:Body>
//           <lib:getBooksRequest>
//              <lib:search>
//                 <lib:orderType>asc</lib:orderType>
//                 <lib:page>1</lib:page>
//                 <lib:pagination>true</lib:pagination>
//                 <lib:size>99</lib:size>
//              </lib:search>
//           </lib:getBooksRequest>
//        </soapenv:Body>
//        </soapenv:Envelope>
        //
        // SOAP Body
        SOAPElement soapBodyElem = soapBody.addChildElement(BookRequestBodyParams.GET_BOOKS_REQUEST.toString(), myNamespace);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(BookRequestBodyParams.SEARCH.toString(), myNamespace);
        //
        SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement(BookRequestBodyParams.ORDER_TYPE.toString(), myNamespace);
        soapBodyElem2.addTextNode("asc");
        //
        SOAPElement soapBodyElem3 = soapBodyElem1.addChildElement(BookRequestBodyParams.PAGE.toString(), myNamespace);
        soapBodyElem3.addTextNode("1");
        //
        SOAPElement soapBodyElem4 = soapBodyElem1.addChildElement(BookRequestBodyParams.PAGINATION.toString(), myNamespace);
        soapBodyElem4.addTextNode("true");
        //
        SOAPElement soapBodyElem5 = soapBodyElem1.addChildElement(BookRequestBodyParams.SIZE.toString(), myNamespace);
        soapBodyElem5.addTextNode("99");
    }

    @Step(value = "create SoapEnvelope to Get a Book by id {0}")
    public void createSoapEnvelopeGetBookById(String id) throws SOAPException {
        logger.info(this.getClass().getSimpleName() + " start createSoapEnvelopeGetBookById()");
        initSoapEnvelope();
        //
//        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lib="libraryService">
//        <soapenv:Header/>
//        <soapenv:Body>
//           <lib:getBookRequest>
//              <lib:bookId>35</lib:bookId>
//           </lib:getBookRequest>
//        </soapenv:Body>
//        </soapenv:Envelope>
        //
        // SOAP Body
        SOAPElement soapBodyElem = soapBody.addChildElement(BookRequestBodyParams.GET_BOOK_REQUEST.toString(), myNamespace);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(BookRequestBodyParams.BOOK_ID.toString(), myNamespace);
        soapBodyElem1.addTextNode(id);
    }

    @Step(value = "create SoapEnvelope to Get a Genre by id {0")
    public void createSoapEnvelopeGetGenre(String id) throws SOAPException {
        logger.info(this.getClass().getSimpleName() + " start createSoapEnvelopeGetGenre()");
        initSoapEnvelope();
        //
//        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lib="libraryService">
//        <soapenv:Header/>
//        <soapenv:Body>
//           <lib:getGenreRequest>
//              <lib:genreId>?</lib:genreId>
//           </lib:getGenreRequest>
//        </soapenv:Body>
//       </soapenv:Envelope>
        //
        // SOAP Body
        SOAPElement soapBodyElem = soapBody.addChildElement(GenreRequestBodyParams.GET_GENRE_REQUEST.toString(), myNamespace);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(GenreRequestBodyParams.GENRE_ID.toString(), myNamespace);
        soapBodyElem1.addTextNode(id);
        //
    }

    @Step(value = "create SoapEnvelope to Get a list of Genres")
    public void createSoapEnvelopeGetGenresList() throws SOAPException {
        logger.info(this.getClass().getSimpleName() + " start createSoapEnvelopeGetGenresList()");
        initSoapEnvelope();
        //
//        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lib="libraryService">
//        <soapenv:Header/>
//        <soapenv:Body>
//           <lib:getGenresRequest>
//              <lib:search>
//                 <lib:orderType>asc</lib:orderType>
//                 <lib:page>1</lib:page>
//                 <lib:pagination>true</lib:pagination>
//                 <lib:size>10</lib:size>
//              </lib:search>
//           </lib:getGenresRequest>
//        </soapenv:Body>
//     </soapenv:Envelope>
        //
        // SOAP Body
        // SOAP Body
        SOAPElement soapBodyElem = soapBody.addChildElement(GenreRequestBodyParams.GET_GENRES_REQUEST.toString(), myNamespace);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(GenreRequestBodyParams.SEARCH.toString(), myNamespace);
        //
        SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement(GenreRequestBodyParams.ORDER_TYPE.toString(), myNamespace);
        soapBodyElem2.addTextNode("asc");
        //
        SOAPElement soapBodyElem3 = soapBodyElem1.addChildElement(GenreRequestBodyParams.PAGE.toString(), myNamespace);
        soapBodyElem3.addTextNode("1");
        //
        SOAPElement soapBodyElem4 = soapBodyElem1.addChildElement(GenreRequestBodyParams.PAGINATION.toString(), myNamespace);
        soapBodyElem4.addTextNode("true");
        //
        SOAPElement soapBodyElem5 = soapBodyElem1.addChildElement(GenreRequestBodyParams.SIZE.toString(), myNamespace);
        soapBodyElem5.addTextNode("99");
        //
    }

    @Step(value = "create SoapEnvelope to create a Genre {0}")
    public void createSoapEnvelopeCreateGenre(GenreEntity genreEntity) throws SOAPException {
        logger.info(this.getClass().getSimpleName() + " start createSoapEnvelopeCreateGenre()");
        initSoapEnvelope();
        //
//        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lib="libraryService">
//        <soapenv:Header/>
//        <soapenv:Body>
//           <lib:createGenreRequest>
//              <lib:genre>
//                 <lib:genreId>?</lib:genreId>
//                 <lib:genreName>?</lib:genreName>
//                 <lib:genreDescription>?</lib:genreDescription>
//              </lib:genre>
//           </lib:createGenreRequest>
//        </soapenv:Body>
//     </soapenv:Envelope>
        //
        // SOAP Body
        // SOAP Body
        SOAPElement soapBodyElem = soapBody.addChildElement(GenreRequestBodyParams.CREATE_GENRE_REQUEST.toString(), myNamespace);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(GenreRequestBodyParams.GENRE.toString(), myNamespace);
        //
        SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement(GenreRequestBodyParams.GENRE_ID.toString(), myNamespace);
        soapBodyElem2.addTextNode(String.valueOf(genreEntity.getGenreId()));
        //
        SOAPElement soapBodyElem3 = soapBodyElem1.addChildElement(GenreRequestBodyParams.GENRE_NAME.toString(), myNamespace);
        soapBodyElem3.addTextNode(genreEntity.getGenreName());
        //
        SOAPElement soapBodyElem4 = soapBodyElem1.addChildElement(GenreRequestBodyParams.GENRE_DESCRIPTION.toString(), myNamespace);
        soapBodyElem4.addTextNode(genreEntity.getGenreDescription());
        //
    }

    @Step(value = "create SoapEnvelope to update a Genre {0}")
    public void createSoapEnvelopeUpdateGenre(GenreEntity genreEntity) throws SOAPException {
        logger.info(this.getClass().getSimpleName() + " start createSoapEnvelopeUpdateGenre()");
        initSoapEnvelope();
        //
//        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lib="libraryService">
//        <soapenv:Header/>
//        <soapenv:Body>
//           <lib:updateGenreRequest>
//              <lib:genre>
//                 <lib:genreId>?</lib:genreId>
//                 <lib:genreName>?</lib:genreName>
//                 <lib:genreDescription>?</lib:genreDescription>
//              </lib:genre>
//           </lib:updateGenreRequest>
//        </soapenv:Body>
//       </soapenv:Envelope>
        //
        // SOAP Body
        // SOAP Body
        SOAPElement soapBodyElem = soapBody.addChildElement(GenreRequestBodyParams.UPDATE_GENRE_REQUEST.toString(), myNamespace);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(GenreRequestBodyParams.GENRE.toString(), myNamespace);
        //
        SOAPElement soapBodyElem2 = soapBodyElem1.addChildElement(GenreRequestBodyParams.GENRE_ID.toString(), myNamespace);
        soapBodyElem2.addTextNode(String.valueOf(genreEntity.getGenreId()));
        //
        SOAPElement soapBodyElem3 = soapBodyElem1.addChildElement(GenreRequestBodyParams.GENRE_NAME.toString(), myNamespace);
        soapBodyElem3.addTextNode(genreEntity.getGenreName());
        //
        SOAPElement soapBodyElem4 = soapBodyElem1.addChildElement(GenreRequestBodyParams.GENRE_DESCRIPTION.toString(), myNamespace);
        soapBodyElem4.addTextNode(genreEntity.getGenreDescription());
        //
    }

    @Step(value = "create SoapEnvelope to delete a Genre {0}")
    public void createSoapEnvelopeDeleteGenre(String id) throws SOAPException {
        logger.info(this.getClass().getSimpleName() + " start createSoapEnvelopeDeleteGenre()");
        initSoapEnvelope();
        //
//        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:lib="libraryService">
//        <soapenv:Header/>
//        <soapenv:Body>
//           <lib:deleteGenreRequest>
//              <lib:genreId>?</lib:genreId>
//              <lib:options>
//                 <lib:forcibly>false</lib:forcibly>
//              </lib:options>
//           </lib:deleteGenreRequest>
//        </soapenv:Body>
//       </soapenv:Envelope>
        //
        // SOAP Body
        // SOAP Body
        SOAPElement soapBodyElem = soapBody.addChildElement(GenreRequestBodyParams.DELETE_GENRE_REQUEST.toString(), myNamespace);
        SOAPElement soapBodyElem1 = soapBodyElem.addChildElement(GenreRequestBodyParams.GENRE_ID.toString(), myNamespace);
        soapBodyElem1.addTextNode(id);
        //
    }
}
