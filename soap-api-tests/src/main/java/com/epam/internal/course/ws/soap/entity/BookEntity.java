package com.epam.internal.course.ws.soap.entity;

public class BookEntity {

    private int bookId;
    private String bookName;
    private String bookLanguage;
    private String bookDescription;
    private int pageCount;
    private double height;
    private double width;
    private double length;
    private int publicationYear;
    //
    public BookEntity() {
        this.bookId = -1;
        this.bookName = "";
        this.bookLanguage = "";
        this.bookDescription = "";
        this.pageCount = -1;
        this.height = -1;
        this.width = -1;
        this.length = -1;
        this.publicationYear = -1;
    }

    public BookEntity(int bookId, String bookName, String bookLanguage, String bookDescription, int pageCount, double height,
            double width, double length, int publicationYear) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.bookLanguage = bookLanguage;
        this.bookDescription = bookDescription;
        this.pageCount = pageCount;
        this.height = height;
        this.width = width;
        this.length = length;
        this.publicationYear = publicationYear;
    }

    @Override
    public String toString() {
        return "BookEntity [bookId=" + bookId + ", bookName=" + bookName + ", bookDescription=" + bookDescription + ", pageCount="
                + pageCount + "]";
    }

    public int getBookId() {
        return bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public String getBookLanguage() {
        return bookLanguage;
    }

    public String getBookDescription() {
        return bookDescription;
    }

    public int getPageCount() {
        return pageCount;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public int getPublicationYear() {
        return publicationYear;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public void setBookLanguage(String bookLanguage) {
        this.bookLanguage = bookLanguage;
    }

    public void setBookDescription(String bookDescription) {
        this.bookDescription = bookDescription;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bookDescription == null) ? 0 : bookDescription.hashCode());
        result = prime * result + bookId;
        result = prime * result + ((bookLanguage == null) ? 0 : bookLanguage.hashCode());
        result = prime * result + ((bookName == null) ? 0 : bookName.hashCode());
        long temp;
        temp = Double.doubleToLongBits(height);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(length);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + pageCount;
        result = prime * result + publicationYear;
        temp = Double.doubleToLongBits(width);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        BookEntity other = (BookEntity) obj;
        if (bookDescription == null) {
            if (other.bookDescription != null)
                return false;
        } else if (!bookDescription.equals(other.bookDescription))
            return false;
        if (bookId != other.bookId)
            return false;
        if (bookLanguage == null) {
            if (other.bookLanguage != null)
                return false;
        } else if (!bookLanguage.equals(other.bookLanguage))
            return false;
        if (bookName == null) {
            if (other.bookName != null)
                return false;
        } else if (!bookName.equals(other.bookName))
            return false;
        if (Double.doubleToLongBits(height) != Double.doubleToLongBits(other.height))
            return false;
        if (Double.doubleToLongBits(length) != Double.doubleToLongBits(other.length))
            return false;
        if (pageCount != other.pageCount)
            return false;
        if (publicationYear != other.publicationYear)
            return false;
        if (Double.doubleToLongBits(width) != Double.doubleToLongBits(other.width))
            return false;
        return true;
    }

}
