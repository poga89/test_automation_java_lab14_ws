package com.epam.internal.course.ws.soap.entity;

public enum ErrorResponseParams {

    FAULT_STRING("faultstring");

    private String param;

    private ErrorResponseParams(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return param;
    }
}
