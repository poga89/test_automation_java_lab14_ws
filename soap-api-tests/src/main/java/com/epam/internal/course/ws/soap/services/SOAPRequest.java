package com.epam.internal.course.ws.soap.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.internal.course.ws.soap.entity.GenreEntity;

import io.qameta.allure.Step;

public class SOAPRequest {
    //
    protected final static Logger logger = LoggerFactory.getLogger(SOAPRequest.class);
    //
    @Step(value = "create SOAP Request to Search a Book By id")
    public static SOAPMessage createSOAPRequestSearchBookByNumber(String soapAction, String number) throws SOAPException, IOException {
        logger.info(SOAPRequest.class.getSimpleName() + " start createSOAPRequestSearchBookByNumber()");
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        new SoapEnvelope(soapMessage).createSoapEnvelopeGetBookById(number);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String requestMsg = new String(out.toByteArray());
        logger.info(SOAPRequest.class.getSimpleName() + " Request SOAP Message: " + requestMsg);

        return soapMessage;
    }
    
    @Step(value = "create SOAP Request to Get a List Of Books")
    public static SOAPMessage createSOAPRequestGetListOfBooks(String soapAction) throws SOAPException, IOException {
        logger.info(SOAPRequest.class.getSimpleName() + " start createSOAPRequestGetListOfBooks()");
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        new SoapEnvelope(soapMessage).createSoapEnvelopeGetBooks();

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String requestMsg = new String(out.toByteArray());
        logger.info(SOAPRequest.class.getSimpleName() + " Request SOAP Message: " + requestMsg);

        return soapMessage;
    }
    
    @Step(value = "create SOAP Request to Get a List Of Genres")
    public static SOAPMessage createSOAPRequestGetListOfGenres(String soapAction) throws SOAPException, IOException {
        logger.info(SOAPRequest.class.getSimpleName() + " start createSOAPRequestGetListOfGenres()");
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        new SoapEnvelope(soapMessage).createSoapEnvelopeGetGenresList();

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String requestMsg = new String(out.toByteArray());
        logger.info(SOAPRequest.class.getSimpleName() + " Request SOAP Message: " + requestMsg);

        return soapMessage;
    }
    
    @Step(value = "create SOAP Request to Search a Genres by id")
    public static SOAPMessage createSOAPRequestSearchGenreByNumber(String soapAction, String number) throws SOAPException, IOException {
        logger.info(SOAPRequest.class.getSimpleName() + " start createSOAPRequestSearchGenreByNumber()");
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        new SoapEnvelope(soapMessage).createSoapEnvelopeGetGenre(number);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String requestMsg = new String(out.toByteArray());
        logger.info(SOAPRequest.class.getSimpleName() + " Request SOAP Message: " + requestMsg);

        return soapMessage;
    }
    
    @Step(value = "create SOAP Request to Create a Genre")
    public static SOAPMessage createSOAPRequestCreateGenre(String soapAction, GenreEntity genreEntity) throws SOAPException, IOException {
        logger.info(SOAPRequest.class.getSimpleName() + " start createSOAPRequestCreateGenre()");
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        new SoapEnvelope(soapMessage).createSoapEnvelopeCreateGenre(genreEntity);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String requestMsg = new String(out.toByteArray());
        logger.info(SOAPRequest.class.getSimpleName() + " Request SOAP Message: " + requestMsg);

        return soapMessage;
    }
    
    @Step(value = "create SOAP Request to Update a Genre")
    public static SOAPMessage createSOAPRequestUpdateGenre(String soapAction, GenreEntity genreEntity) throws SOAPException, IOException {
        logger.info(SOAPRequest.class.getSimpleName() + " start createSOAPRequestUpdateGenre()");
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        new SoapEnvelope(soapMessage).createSoapEnvelopeUpdateGenre(genreEntity);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String requestMsg = new String(out.toByteArray());
        logger.info(SOAPRequest.class.getSimpleName() + " Request SOAP Message: " + requestMsg);

        return soapMessage;
    }
    
    @Step(value = "create SOAP Request to Delete a Genre")
    public static SOAPMessage createSoapEnvelopeDeleteGenre(String soapAction, String number) throws SOAPException, IOException {
        logger.info(SOAPRequest.class.getSimpleName() + " start createSoapEnvelopeDeleteGenre()");
        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();

        new SoapEnvelope(soapMessage).createSoapEnvelopeDeleteGenre(number);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);

        soapMessage.saveChanges();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        soapMessage.writeTo(out);
        String requestMsg = new String(out.toByteArray());
        logger.info(SOAPRequest.class.getSimpleName() + " Request SOAP Message: " + requestMsg);

        return soapMessage;
    }
}
