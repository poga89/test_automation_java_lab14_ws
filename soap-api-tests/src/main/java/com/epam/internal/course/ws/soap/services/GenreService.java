package com.epam.internal.course.ws.soap.services;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epam.internal.course.ws.soap.entity.GenreEntity;
import com.epam.internal.course.ws.soap.entity.ResponseErrorMessage;
import com.epam.internal.course.ws.soap.utils.ReadProjectProperties;
import com.epam.internal.course.ws.soap.utils.RegexUtils;
import com.epam.internal.course.ws.soap.utils.RsponseParser;

import io.qameta.allure.Step;

public class GenreService {
    //
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private static final ReadProjectProperties READ_PROJECT_PROPERTIES = new ReadProjectProperties();
    //
    private static String soapEndpointUrl = READ_PROJECT_PROPERTIES.getSoapEndpointUrl();
    //
    private SOAPConnectionFactory soapConnectionFactory;
    private SOAPConnection soapConnection;
    private SOAPMessage soapResponse;
    private ResponseErrorMessage responseErrorMessage;

    public GenreService() {
    }

    // getGenres
    @Step(value = "get a list of genres")
    public List<GenreEntity> getGenresList() {
        logger.info(this.getClass().getSimpleName() + " start getGenresList()");
        List<GenreEntity> genreEntities = new ArrayList<GenreEntity>();
        try {
            // Create SOAP Connection
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
            logger.info(this.getClass().getSimpleName() + " Create SOAP Connection for getting a list of genres");

            // Send SOAP Message to SOAP Server
            soapResponse = soapConnection.call(SOAPRequest.createSOAPRequestGetListOfGenres(soapEndpointUrl + GenreEndpoints.GET_GENRES.toString()),
                    soapEndpointUrl);
            logger.info(this.getClass().getSimpleName() + " Send SOAP Message to SOAP Server for getting a list of genres");

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            FileOutputStream fos = new FileOutputStream(new File(READ_PROJECT_PROPERTIES.getMessageDataFilePath()));
            out.writeTo(fos);
            String strMsg = new String(out.toByteArray());
            //
            logger.info(this.getClass().getSimpleName() + " Response: " + strMsg);
            genreEntities = RsponseParser.getListOfGenre(strMsg);
            //
            soapConnection.close();
            logger.info(this.getClass().getSimpleName() + " soapConnection.close()");
        } catch (Exception e) {
            logger.error(
                    "\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return genreEntities;
    }

    // getGenre
    @Step(value = "get a genre by id {0}")
    public GenreEntity getGenreById(int id) {
        logger.info(this.getClass().getSimpleName() + " start getGenresList()");
        GenreEntity genreEntity = new GenreEntity();
        try {
            // Create SOAP Connection
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
            logger.info(this.getClass().getSimpleName() + " Create SOAP Connection for getting a genre by id");

            // Send SOAP Message to SOAP Server
            soapResponse = soapConnection
                    .call(SOAPRequest.createSOAPRequestSearchGenreByNumber(soapEndpointUrl + GenreEndpoints.GET_GENRE.toString(), String.valueOf(id)), soapEndpointUrl);
            logger.info(this.getClass().getSimpleName() + " Send SOAP Message to SOAP Server for getting a genre by id");

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            String strMsg = new String(out.toByteArray());
            logger.info(this.getClass().getSimpleName() + " response: " + strMsg);
            //
            genreEntity = RsponseParser.getGenre(strMsg);
            //
            soapConnection.close();
            logger.info(this.getClass().getSimpleName() + " soapConnection.close()");
        } catch (Exception e) {
            logger.error(
                    "\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return genreEntity;
    }

    // createGenre
    @Step(value = "create a genre {0}")
    public GenreEntity createGenre(GenreEntity genreEntity) {
        logger.info(this.getClass().getSimpleName() + " start createGenre()");
        GenreEntity genreEntityNew = new GenreEntity();
        try {
            // Create SOAP Connection
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
            logger.info(this.getClass().getSimpleName() + " Create SOAP Connection for creating a genre");

            // Send SOAP Message to SOAP Server
            soapResponse = soapConnection
                    .call(SOAPRequest.createSOAPRequestCreateGenre(soapEndpointUrl + GenreEndpoints.CREATE_GENRE.toString(), genreEntity), soapEndpointUrl);
            logger.info(this.getClass().getSimpleName() + " Send SOAP Message to SOAP Server for creating a genre");

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            String strMsg = new String(out.toByteArray());
            logger.info(this.getClass().getSimpleName() + " response: " + strMsg);
            //
            genreEntityNew = RsponseParser.getGenre(strMsg);
            //
            soapConnection.close();
            logger.info(this.getClass().getSimpleName() + " soapConnection.close()");
        } catch (Exception e) {
            logger.error(
                    "\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return genreEntityNew;
    }

    // updateGenre
    @Step(value = "update a genre {0}")
    public GenreEntity updateGenre(GenreEntity genreEntity) {
        logger.info(this.getClass().getSimpleName() + " start updateGenre()");
        GenreEntity genreEntityUpdated = new GenreEntity();
        try {
            // Create SOAP Connection
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
            logger.info(this.getClass().getSimpleName() + " Create SOAP Connection for updaing a genre");

            // Send SOAP Message to SOAP Server
            soapResponse = soapConnection
                    .call(SOAPRequest.createSOAPRequestUpdateGenre(soapEndpointUrl + GenreEndpoints.UPDATE_GENRE.toString(), genreEntity), soapEndpointUrl);
            logger.info(this.getClass().getSimpleName() + " Send SOAP Message to SOAP Server for updaing a genre");

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            String strMsg = new String(out.toByteArray());
            logger.info(this.getClass().getSimpleName() + " response: " + strMsg);
            //
            genreEntityUpdated = RsponseParser.getGenre(strMsg);
            //
            soapConnection.close();
            logger.info(this.getClass().getSimpleName() + " soapConnection.close()");
        } catch (Exception e) {
            logger.error(
                    "\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return genreEntityUpdated;
    }
    
 // deleteGenre
    public String deleteGenre(GenreEntity genreEntity) {
        logger.info(this.getClass().getSimpleName() + " start deleteGenre()");
        String response = "";
        try {
            // Create SOAP Connection
            soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
            logger.info(this.getClass().getSimpleName() + " Create SOAP Connection for deleting a genre by id");

            // Send SOAP Message to SOAP Server
            soapResponse = soapConnection
                    .call(SOAPRequest.createSoapEnvelopeDeleteGenre(soapEndpointUrl + GenreEndpoints.DELETE_GENRE.toString(), String.valueOf(genreEntity.getGenreId())), soapEndpointUrl);
            logger.info(this.getClass().getSimpleName() + " Send SOAP Message to SOAP Server for deleting a genre");

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            soapResponse.writeTo(out);
            String strMsg = new String(out.toByteArray());
            logger.info(this.getClass().getSimpleName() + " response: " + strMsg);
            //
            response = RegexUtils.getDataByTag(strMsg, "status");
            logger.info(this.getClass().getSimpleName() + " response status: " + response);
            if(response.length()==0) {
                responseErrorMessage = new ResponseErrorMessage(RegexUtils.getErrorText(strMsg));
                logger.error(this.getClass().getSimpleName() + " response status: " + response);
            }
            //
            soapConnection.close();
            logger.info(this.getClass().getSimpleName() + " soapConnection.close()");
        } catch (Exception e) {
            logger.error(
                    "\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
        return response;
    }

    @Step(value = "checking if a genres list {0} contains certain GenreId {0}")
    public boolean checkIfGenreListContainGenreId(List<GenreEntity> genreEntities, int id) {
        logger.info(this.getClass().getSimpleName() + " checking if a genres list contains certain GenreId");
        for (GenreEntity genreEntity : genreEntities) {
            if (genreEntity.getGenreId() == id) {
                return true;
            }
        }
        return false;
    }

    @Step(value = "checking if a genres list contains certain GenreId and correcting new genre id {0}")
    public GenreEntity checkAndCorrectNewGenreID(GenreEntity genreEntity) {
        logger.info(
                this.getClass().getSimpleName() + " checking if a genres list contains certain GenreId and correcting new genre id");
        List<GenreEntity> genres = getGenresList();
        for (GenreEntity genreEntity2 : genres) {
            if (genreEntity.getGenreId() == genreEntity2.getGenreId()) {
                genreEntity.setGenreId(genreEntity.getGenreId() + 1);
                break;
            }
        }
        return genreEntity;
    }
    
    public String getResponseErrorMessage() {
        return responseErrorMessage.getErrorMessage();
    }

}
