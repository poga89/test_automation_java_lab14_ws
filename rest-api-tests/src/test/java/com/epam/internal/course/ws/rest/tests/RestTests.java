package com.epam.internal.course.ws.rest.tests;

import java.util.List;

import org.apache.http.HttpStatus;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.epam.internal.course.ws.rest.data.DataRepository;
import com.epam.internal.course.ws.rest.entity.GenreEntity;
import com.epam.internal.course.ws.rest.services.GenreService;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;

public class RestTests extends TestRunner {

    @DataProvider
    public Object[][] getTestGenre() {
        return new Object[][] { { DataRepository.get().getNewGenre() }, };
    }

    @DataProvider
    public Object[][] getUpdatedTestGenre() {
        return new Object[][] { { DataRepository.get().updateNewGenre() }, };
    }

    @Epic("checkGenresList")
    @Feature(value = "Check for getting a list of genres")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check getting a list of genres")
    @Story(value = "Check getting a list of genres")
    @Test
    public void checkGenresList() throws ParseException {
        GenreService genreService = new GenreService();
        List<GenreEntity> genreEntities = genreService.getGenresList();
        this.genreEntity = genreEntities.get(0);
        Assert.assertEquals(genreService.getResponseCode(), HttpStatus.SC_OK, "a request wasn't successful");
        Assert.assertTrue(genreEntities.size() > 0, "a list of genres doesn't contain any records");
        Assert.assertTrue(genreEntities.get(0).getGenreId() > 0, "a list of genres contains wrong records");
    }

    @Epic("checkGenreById")
    @Feature(value = "Check for getting a genre by id")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for getting a genre by id")
    @Story(value = "Check for getting a genre by id")
    @Test
    public void checkGenreById() {
        GenreService genreService = new GenreService();
        GenreEntity genreEntity = genreService.getGenreByNumber(this.genreEntity.getGenreId());
        Assert.assertEquals(genreService.getResponseCode(), HttpStatus.SC_OK, "a request wasn't successful");
        Assert.assertTrue(!genreEntity.getGenreName().isEmpty(), "a genre contains wrong name");
    }

    // negative test
    @Epic("checkGenreByWrongId")
    @Feature(value = "Negative test. Check for getting a genre by a wrong id")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for getting a genre by id")
    @Story(value = "Check for getting a genre by id")
    @Test
    public void checkGenreByWrongId() {
        GenreService genreService = new GenreService();
        genreService.getGenreByNumber(this.genreEntity.getGenreId() - 1);
        Assert.assertTrue(genreService.getResponseCode() == HttpStatus.SC_BAD_REQUEST || genreService.getResponseCode() == HttpStatus.SC_NOT_FOUND,
                "get a success request instead of a wrong");
    }

    @Epic("createTestGenre")
    @Feature(value = "Check for creating a TestGenre")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for creating a TestGenre")
    @Story(value = "Check for creating a TestGenre")
    @Test(dataProvider = "getTestGenre")
    public void createTestGenre(GenreEntity genreEntity) throws ParseException {
        GenreService genreService = new GenreService();
        genreEntity = genreService.checkAndCorrectNewGenreID(genreEntity);
        this.genreEntity = genreEntity;
        GenreEntity newGenreEntity = genreService.createNewGenre(genreEntity);
        Assert.assertEquals(genreService.getResponseCode(), HttpStatus.SC_CREATED, "a request wasn't successful");
        Assert.assertTrue(!newGenreEntity.getGenreName().isEmpty(), "a genre contains a wrong name");
        Assert.assertEquals(newGenreEntity, genreEntity, "a genre contains wrong data");
    }

    @Epic("updateTestGenre")
    @Feature(value = "Check for updatint a TestGenre")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for updating a TestGenre")
    @Story(value = "Check for updating a TestGenre")
    @Test(dataProvider = "getUpdatedTestGenre")
    public void updateTestGenre(GenreEntity genreEntity) {
        GenreService genreService = new GenreService();
        genreEntity.setGenreId(this.genreEntity.getGenreId());
        GenreEntity genreEntityUpdated = genreService.updateGenre(genreEntity);
        Assert.assertEquals(genreService.getResponseCode(), HttpStatus.SC_OK, "a request wasn't successful");
        Assert.assertEquals(genreEntityUpdated, genreEntity, "a genre contains wrong data");
    }

    @Epic("deleteTestGenre")
    @Feature(value = "Check for deleting a TestGenre")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for deleting a TestGenre")
    @Story(value = "Check for deleting a TestGenre")
    @Test(dataProvider = "getTestGenre")
    public void deleteTestGenre(GenreEntity genreEntity) {
        GenreService genreService = new GenreService();
        genreEntity.setGenreId(this.genreEntity.getGenreId());
        int statusCode = genreService.deleteGenreById(genreEntity);
        Assert.assertEquals(statusCode, HttpStatus.SC_NO_CONTENT, "a request wasn't successful");
    }

    // negative test
    @Epic("deleteTestGenreByWrongId")
    @Feature(value = "Negative test. Check for deleting a genre by a wrong id")
    @Severity(SeverityLevel.TRIVIAL)
    @Description("Check for deleting a genre by a wrong id")
    @Story(value = "Check for deleting a genre by a wrong id")
    @Test(dataProvider = "getTestGenre")
    public void deleteTestGenreByWrongId(GenreEntity genreEntity) {
        GenreService genreService = new GenreService();
        genreEntity.setGenreId(this.genreEntity.getGenreId() + 1);
        int statusCode = genreService.deleteGenreById(genreEntity);
        Assert.assertTrue(statusCode == HttpStatus.SC_BAD_REQUEST || statusCode == HttpStatus.SC_NOT_FOUND, "a genre deleted successfully");
    }
}
