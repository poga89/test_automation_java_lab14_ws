package com.epam.internal.course.ws.rest.services;

import org.json.simple.JSONObject;

import com.epam.internal.course.ws.rest.entity.GenreEntity;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;

public class RequestParams {
    
    private RequestSpecification requestSpec;
    
    public RequestParams() {
        requestSpec = RestAssured.given();
    }

    public static RequestSpecification getUriParamsForGenreList(RequestSpecification requestSpec) {
        requestSpec.param(GenreRequestParams.ORDER_TYPE.toString(), GenreRequestParams.ASC.toString())
                    .param(GenreRequestParams.PAGE.toString(), "1")
                    .param(GenreRequestParams.PAGINATION.toString(), GenreRequestParams.TRUE.toString())
                    .param(GenreRequestParams.SIZE.toString(), "100")
                    .param(GenreRequestParams.SORT_BY.toString(), GenreRequestParams.GENRE_ID.toString());
        return requestSpec;
    }
    
    public static RequestSpecification getUriParamsForDeleteGenre(RequestSpecification requestSpec) {
        return requestSpec.param(GenreRequestParams.FORCIBLY.toString(), GenreRequestParams.FALSE.toString());
    }
    
    public static RequestSpecification getUriParamsForBookList(RequestSpecification requestSpec) {
        requestSpec.param("orderType", "asc")
                    .param("page", "1")
                    .param("pagination", "true")
                    .param("size", "100")
                    .param("sortBy","bookId");
        return requestSpec;
    }
    
    @SuppressWarnings("unchecked")
    public static String getBodyParamsForCreateUpdateGenre(GenreEntity genreEntity) {
        JSONObject requestPaarams = new JSONObject();
        requestPaarams.put(GenreRequestParams.GENRE_ID.toString(), (long)genreEntity.getGenreId());
        requestPaarams.put(GenreRequestParams.GENRE_NAME.toString(), genreEntity.getGenreName());
        requestPaarams.put(GenreRequestParams.GENRE_DESCRIPTION.toString(), genreEntity.getGenreDescription());
        return requestPaarams.toJSONString();
    }
    
    public RequestParams orderTypeByAsc() {
        requestSpec.param(GenreRequestParams.ORDER_TYPE.toString(), GenreRequestParams.ASC.toString());
        return this;
    }

    public RequestParams orderTypeByDesc() {
        requestSpec.param(GenreRequestParams.ORDER_TYPE.toString(), GenreRequestParams.DESC.toString());
        return this;
    }

    public RequestParams pages(int pages) {
        requestSpec.param(GenreRequestParams.PAGE.toString(), String.valueOf(pages));
        return this;
    }

    public RequestParams paginationTrue() {
        requestSpec.param(GenreRequestParams.PAGE.toString(), GenreRequestParams.TRUE.toString());
        return this;
    }

    public RequestParams paginationFalse() {
        requestSpec.param(GenreRequestParams.PAGE.toString(), GenreRequestParams.FALSE.toString());
        return this;
    }

    public RequestParams size(int size) {
        requestSpec.param(GenreRequestParams.SIZE.toString(), String.valueOf(size));
        return this;
    }

    public RequestParams sortByGenreId() {
        requestSpec.param(GenreRequestParams.SIZE.toString(), GenreRequestParams.GENRE_ID.toString());
        return this;
    }

    public RequestSpecification get() {
        return requestSpec;
    }

}
