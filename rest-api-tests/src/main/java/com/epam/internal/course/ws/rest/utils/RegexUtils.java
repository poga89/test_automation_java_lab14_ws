package com.epam.internal.course.ws.rest.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtils {

    public static String getDataByTag(String response, String tag) { // tools > RegexUtils
        final Matcher matcher = Pattern.compile(".*" + tag + ">(.+)</ns2:" + tag + ".*").matcher(response);
        String tagData = "";
        while (matcher.find()) {
            tagData = matcher.group(1);
        }
        return tagData;
    }

    public static String getErrorText(String response) {
        final Matcher matcher = Pattern.compile(".*faultstring xml:lang=\"en\">(.+)</faultstring.*").matcher(response);
        String tagData = "";
        while (matcher.find()) {
            response = matcher.group(1);
            tagData = matcher.group(1);
        }
        return tagData;
    }

    public static List<String> getListOfDataByTag(String response, String tag) { // tools > RegexUtils
        Pattern pattern = Pattern.compile("ns2:" + tag + "\\>");
        Matcher matcher = pattern.matcher(response);
        List<String> listStr = new ArrayList<String>();
        int count = 0;
        int previous = 0;
        int next = 0;
        while (matcher.find()) {
            count++;
            next = matcher.start();
            if (count > 1 && count % 2 == 0) {
                listStr.add(response.substring(previous, next - 2));
            }
            previous = matcher.end();
        }
        return listStr;
    }
}