package com.epam.internal.course.ws.rest.entity;

public class GenreEntity {
    private int genreId;
    private String genreName;
    private String genreDescription;

    public GenreEntity(int genreId, String genreName, String genreDescription) {
        this.genreId = genreId;
        this.genreName = genreName;
        this.genreDescription = genreDescription;
    }

    public GenreEntity() {
        this.genreId = -1;
        this.genreName = "";
        this.genreDescription = "";
    }

    @Override
    public String toString() {
        return "GenreEntity [genreId=" + genreId + ", genreName=" + genreName + ", genreDescription=" + genreDescription + "]";
    }

    public int getGenreId() {
        return genreId;
    }

    public String getGenreName() {
        return genreName;
    }

    public String getGenreDescription() {
        return genreDescription;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    public void setGenreDescription(String genreDescription) {
        this.genreDescription = genreDescription;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((genreDescription == null) ? 0 : genreDescription.hashCode());
        result = prime * result + genreId;
        result = prime * result + ((genreName == null) ? 0 : genreName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GenreEntity other = (GenreEntity) obj;
        if (genreDescription == null) {
            if (other.genreDescription != null)
                return false;
        } else if (!genreDescription.equals(other.genreDescription))
            return false;
        if (genreId != other.genreId)
            return false;
        if (genreName == null) {
            if (other.genreName != null)
                return false;
        } else if (!genreName.equals(other.genreName))
            return false;
        return true;
    }

}
