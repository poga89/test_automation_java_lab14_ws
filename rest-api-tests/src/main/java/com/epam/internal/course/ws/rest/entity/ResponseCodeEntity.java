package com.epam.internal.course.ws.rest.entity;

public class ResponseCodeEntity {

    private int responseCode;

    public ResponseCodeEntity() {
        responseCode = -1;
    }

    public ResponseCodeEntity(int responseCode) {
        this.responseCode = responseCode;
    }

    public int getResponseCode() {
        return responseCode;
    }

    @Override
    public String toString() {
        return "ResponseEntity [responseCode=" + responseCode + "]";
    }

}
